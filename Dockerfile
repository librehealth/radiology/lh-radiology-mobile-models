FROM ubuntu:18.04

ENV RPI_QEMU_KERNEL kernel-qemu-4.14.79-stretch
ENV RPI_QEMU_KERNEL_COMMIT c5a491c093604a71db2f01b8fab72bad0e96e2b5
ENV RPI_QEMU_KERNEL_VPB_COMMIT ea19dc94bc7420675b505c81d2c262ee0eacbb0e
ENV RASPBIAN_IMAGE 2019-04-08-raspbian-stretch
ENV RASPBIAN_IMAGE_URL https://downloads.raspberrypi.org/raspbian/images/raspbian-2019-04-09/
WORKDIR /root

# Install dependencies
RUN set -x \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        busybox \
        curl \
        qemu \
        libguestfs-tools \
        unzip \
        linux-image-generic \
        netcat \
    && rm -rf /var/lib/apt/lists/

# Download image, kernel and DTB
RUN set -x \
    && curl -O $RASPBIAN_IMAGE_URL/$RASPBIAN_IMAGE.zip \
    && unzip $RASPBIAN_IMAGE.zip \
    && rm $RASPBIAN_IMAGE.zip \
    && curl https://raw.githubusercontent.com/dhruvvyas90/qemu-rpi-kernel/$RPI_QEMU_KERNEL_COMMIT/$RPI_QEMU_KERNEL > kernel-qemu-stretch \
    && curl -O https://raw.githubusercontent.com/dhruvvyas90/qemu-rpi-kernel/$RPI_QEMU_KERNEL_VPB_COMMIT/versatile-pb.dtb

# Convert image to qcow2, resize it and enable SSH
RUN set -x \
    && qemu-img convert -f raw -O qcow2 $RASPBIAN_IMAGE.img raspbian-stretch.qcow2 \
    && rm $RASPBIAN_IMAGE.img \
    && qemu-img resize raspbian-stretch.qcow2 +2G \
    && guestfish --rw -m /dev/sda1 -a raspbian-stretch.qcow2 write /ssh ""

EXPOSE 2222

HEALTHCHECK CMD ["nc", "-z", "-w5", "localhost", "2222"]

CMD ["qemu-system-arm", "-kernel", "kernel-qemu-stretch", "-append", "root=/dev/sda2 rootfstype=ext4 rw'", "-hda", "raspbian-stretch.qcow2", "-cpu", "arm1176", "-m", "256", "-machine", "versatilepb", "-no-reboot", "-dtb", "versatile-pb.dtb", "-nographic", "-net", "user,hostfwd=tcp::2222-:22", "-net", "nic"]

# Command to run - sudo docker run -it -p 2222:2222 --privileged <image-name>
