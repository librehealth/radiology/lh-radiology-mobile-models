# lh-radiology-mobile-models

Repository for low powered models for classification and detection.

https://forums.librehealth.io/t/project-low-powered-models-for-disease-detection-and-classification-for-radiology-images/3706

## Steps to setup the repository

1. Clone the repo and install the requirements using - `pip3 install requirements.txt`.
Download the RSNA Pneumonia Detection Dataset - https://www.kaggle.com/c/rsna-pneumonia-detection-challenge/data . 
Download the Chest-XRay14 dataset from Kaggle - https://www.kaggle.com/nih-chest-xrays/data 
After downloading the dataset, put all the Chest-XRay14 images in a single folder named `images`. The data-split has been provided in the csv files present in the `data_split` folder.

2. Set up the directory structure and create the directories - quantized-models, trained-models and png-images.
```
├── Chest-XRay14-Scripts
    ├── quantized-models
    ├── pruned-models
    ├── images
    ├── data_split
├── RSNA-Pneumonia-Detection-Scripts
    ├── stage_2_train_images
    ├── stage_2_train_labels.csv
    ├── trained-models
    ├── quantized-models
    ├── pruned-models
    ├── png-images
    │   ├── train
    │   │   ├── 0
    │   │   └── 1
    │   ├── val
    │   │   ├── 0
    │   │   └── 1
    │   ├── test
    │   │   ├── 0
    │   │   └── 1
```

Link to weights of all the models - [Click here](https://drive.google.com/drive/folders/1uStW6m3LZ3A2iYT8Xzl3yCWnzUuzrmhr?usp=sharing)

## Steps to run RSNA Pneumonia Detection scripts

1. Link to pretrained models for RSNA Pneumonia Detection - [Click here](https://drive.google.com/drive/folders/1ZNo5jQAlJxvWN6xeaR6TBfUP4FlaZL7T?usp=sharing). Download them in the `trained-models` folder.

2. Preprocess the data using `data-preprocessing.py`. 
This file accepts the DICOM images, converts them to png format and saves them in train, val and test directories. This file accepts 5 positional agruments -
    1. CSV - path to csv file `stage_2_train_labels.csv`.
    2. dcmdir - path to DICOM image folder `stage_2_train_images`.
    3. traindir - path to training images `png-images/train`.
    4. valdir - path to validation images `png-images/val`.
    5. testdir - path to test images `png-images/test`.

    To run use the absolute path to each folder/file. 
    Command - `python3 data-preprocessing.py /path/to/stage_2_train_labels.csv /path/to/stage_2_train_images /path/to/png-images/train /path/to/png-images/val /path/to/png-images/test`

3. Train the model using the `train.py` script.
This file accepts the type of model to train, number to epochs to train for and saves them to a weights folder. It accepts 2 positional arguments -
    1. weightsfolder - path to folder where models and their weights will be saved `trained-models`.
    2. epochs - number of epochs to train the model.

    Use the absolute paths to all the folders.
    Command - `python3 train.py /path/to/trained-models 10`
    where 10 is the number of epochs for training.

    Upon running this script, the terminal will ask the user to enter the type of the model to train. Choose either `densenet` or `inception`. Then the model will be created, trained as well as evaluated.

4. Quantization of models using `quantize.py` scrpit.
This file accepts the model, its weights and quantizes them to the required format and stores them in the `quantized-models` directory.
It accepts 1 positional argument - 
    1. tflitepath - Path to save the quantized models `quantized-models`

    Command - `python3 quantize.py /path/to/quantized-models`.
    Upon running this, the terminal will ask the user to enter the model type to quantize. Choose either `densenet` , `inception` or `pruned`. Then, enter the path to the trained model `/path/to/model.h5`. Next, the user will be asked to enter the type of quantization -
        1. Normal tflite conversion 
        2. Dynamic quantization 
        3. Float-16 quantization
        4. Int-8 quantization
    Enter your choice 1, 2, 3 or 4 and the script will quantize your model, evaulate it and save it.

5. Pruning models using `prune.py` script - 
This file accepts the model, it's weights and prunes the model to save in the `pruned-models` directory. It accepts 2 positional arguments - 
    1. weightsfile - Path to saved weights either in .h5 or .hdf5 format `trained-models/weights.hdf5`
    2. prunepath - Path to save the pruned models `pruned-models`

    Command - `python3 prune.py /path/to/trained-models/weights.hdf5 /path/to/pruned-models`.
    Upon running this, the terminal will ask the user to enter the model type to prune. Choose either `densenet` or `inception`. After this, the script will prune the model, evaluate it and save it.

6. Inference using `inference.py` script - 
This file accepts the image to run inference upon, and the model to run inference. It accepts 1 positional argument -
    1. imagepath - Path to PNG image to classify.

    Command - `python3 inference.py /path/to/image.png`
    The image can be of sizes - 1024x1024, 224x224, 1024x1024x3 or 224x224x3.
    Then, enter the model to run inference by picking from `densenet`, `inception`, `pruned` and `quantized` models. Accordingly, enter the `/path/to/model.h5` or `/path/to/model.tflite`. The script will return the class of the image.


## Steps to run Chest-XRay14 Scripts

1. The pretrained model has been taken from Bruce Chou's implementation of CheXNet. Download the pre-trained weights from [here](https://github.com/brucechou1983/CheXNet-Keras). The link to weights is present in the Readme.

2. Quantize models using `quantize.py` script -
This file accepts the model, its weights and quantizes them to the required format and stores them in the `quantized-models` directory.
It accepts 1 positional argument - 
    1. imgsourcedir - Path to folder conatining all `images`
    2. tflitepath - Path to save the quantized models `quantized-models`

    Command - `python3 quantize.py /path/to/images /path/to/quantized-models`
    Upon running this, the terminal will ask the user to enter the model type to quantize. Choose either `chexnet` or `pruned`. Then, enter the path to the trained model `/path/to/model.h5`. Next, the user will be asked to enter the type of quantization -
        1. Normal tflite conversion 
        2. Dynamic quantization 
        3. Float-16 quantization
        4. Int-8 quantization
    Enter your choice 1, 2, 3 or 4 and the script will quantize your model, evaulate it and save it.

3. Prune models using `prune.py` script - 
This file accepts the model, it's weights and prunes the model to save in the `pruned-models` directory. It accepts 2 positional arguments - 
    1. weightsfile - Path to saved weights either in .h5 or .hdf5 format `trained-models/weights.hdf5`
    2. imgsourcedir - Path to folder conatining all `images`
    3. prunepath - Path to save the pruned models `pruned-models`

    Command - `python3 prune.py /path/to/trained-models/weights.hdf5 /path/to/images /path/to/pruned-models`.
    After this, the script will prune the model, evaluate it and save it.

4. Evaluate models using `evalute_model.py` script -
This file accepts the model and the path to the images and returns results of the evaluated model. It accepts 1 positional argument -
    1. imgsourcedir - Path to folder conatining all `images`

    Command - `python3 evaluate_model.py /path/to/images`
    Upon running this, the terminal will ask you the type of model to evaluate and the path to the trained model `path/to/model.h5`. Enter the path and the script will return the AUC-ROC score of that model.

4. Inference using `inference.py` script - 
This file accepts the image to run inference upon, and the model to run inference. It accepts 1 positional argument -
    1. imagepath - Path to PNG image to classify.

    Command - `python3 inference.py /path/to/image.png`
    The image can be of sizes - 1024x1024, 224x224, 1024x1024x3 or 224x224x3.
    Then, enter the model to run inference by picking from `chexnet`, `pruned` and `quantized` models. Accordingly, enter the `/path/to/model.h5` or `/path/to/model.tflite`. The script will return the class of the image.

### Commands to create Sphinx documentation
After quickstarting sphinx, run the following commands to create documentation for all the scripts in the respective subdirectories. 
Make sure that autodoc is enabled.

`sphinx-apidoc -o Chest-Xray14 /path/to/lh-radiology-mobile-models/Chest-XRay14-Scripts/`

`sphinx-apidoc -o RSNA-Pneumonia-Detection /path/to/lh-radiology-mobile-models/RSNA-Pneumonia-Detection-Scripts/`

## References

### Datasets 
1. RSNA Pneumonia Detection - https://www.kaggle.com/c/rsna-pneumonia-detection-challenge/data
2. Chest-XRay14 - https://www.kaggle.com/nih-chest-xrays/data

### Model Compression
1. https://www.tensorflow.org/lite/performance/model_optimization
2. https://youtu.be/3JWRVx1OKQQ
3. https://youtu.be/4iq-d2AmfRU

### Model and Weights
1. ChexNet - https://github.com/brucechou1983/CheXNet-Keras