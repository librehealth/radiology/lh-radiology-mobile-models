from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.optimizers import Adam
import tensorflow as tf
import cv2, timeit
import argparse
import numpy as np


def main(args):
    """ 
    Description - Main function
    :type args: list
    :param args: List of arguments to parse
    """
    
    print("Enter model type to run inference - 1. Chexnet, 2. Pruned, 3. TFlite")
    model_type = int(input())
    if model_type == 1:
        model_name = "chexnet"
    elif model_type == 2:
        model_name = "pruned"
    elif model_type == 3:
        model_name = "quantized"
    else:
        raise Exception("Incorrect model name")

    class_names = [ 'Atelectasis', 'Cardiomegaly', 'Effusion', 'Infiltration', 'Mass', 'Nodule', 'Pneumonia',
                'Pneumothorax', 'Consolidation', 'Edema', 'Emphysema', 'Fibrosis', 'Pleural_Thickening', 'Hernia']
    img = load_img(args.imagepath)
    img = img_to_array(img)
    img = cv2.resize(img,(224,224)) /255

    if (model_name == "chexnet" or model_name == "pruned"):
        # weights.hdf5 or model.h5 is the expected input here
        # model = build_model(model_name)
        test_image = np.expand_dims(img, axis=0).astype(np.float32)
        print("Enter path to saved model")
        weightsfile = str(input())
        start = timeit.default_timer()
        model = tf.keras.models.load_model(weightsfile)
        prediction = model.predict(test_image)
        stop = timeit.default_timer()
        for i in  range(len(class_names)):
            print(f"{class_names[i]}: {prediction[0][i]}\n")
        print('Time taken: ', stop - start)

    elif model_name == "quantized":
        print("Enter 1 if model is int8 format else enter 2")
        flag = int(input())
        if(flag==1):
            test_image = np.expand_dims(img, axis=0).astype(np.uint8)
        elif(flag==2):
            test_image = np.expand_dims(img, axis=0).astype(np.float32)
        else:
            raise Exception("Incorrect input")
        print("Enter path to tflite model file (.tflite format)")
        weightsfile = str(input())
        start = timeit.default_timer()
        interpreter = tf.lite.Interpreter(model_path=str(weightsfile))
        interpreter.allocate_tensors()
        input_index = interpreter.get_input_details()[0]["index"]
        output_index = interpreter.get_output_details()[0]["index"]

        interpreter.set_tensor(input_index, test_image)
        interpreter.invoke()
        prediction = interpreter.get_tensor(output_index)
        stop = timeit.default_timer()
        for i in  range(len(class_names)):
            print(f"{class_names[i]}: {prediction[0][i]}\n")
        print('Time taken: ', stop - start)

    else:
        raise Exception("Incorrect model name")

    print("Done")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('imagepath', metavar="image", type=str, help="Image to perform detection")
    args = parser.parse_args()
    main(args)