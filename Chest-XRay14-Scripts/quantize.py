import tensorflow as tf
import pathlib, os, argparse
import sys
import numpy as np
import pandas as pd
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from generator import AugmentedImageSequence
from model_build import model_builder
from evaluate_model import evaluate_tflite_model, evaluate_int8_tflite_model
from tqdm import tqdm



    
def model_converter(model, conversion_type, tflite_models_dir, model_name, test_sequence):
    
    """ 
    Description - Converts model to quantized format
    :type model: tensorflow model
    :param model: Model to be converted
    
    :type conversion_type: int
    :param conversion_type: Type of quantized model to obtain
    
    :type tflite_models_dir: str
    :param tflite_models_dir: Directory to store the tflite model

    :type model_name: str
    :param model_name: Name of model to quantize

    :type test_sequence: generator
    :param test_sequence: Test data generator
    
    :rtype: str, str
    """
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    if(conversion_type == 1):
        converter_name = "generic"
    elif(conversion_type == 2):
        converter_name = "dynamic"
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
    elif(conversion_type == 3):
        converter_name = "float16"
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        converter.target_spec.supported_types = [tf.float16]
    elif(conversion_type == 4):
        converter_name = "int8"
        val_images, _ = tqdm(next(iter(test_sequence)))
        images = np.expand_dims(val_images[0], axis=0).astype(np.float32)
        val_ds = tf.data.Dataset.from_tensor_slices((images)).batch(1)

        def representative_data_gen():
            for input_value in val_ds.take(100):
                yield [input_value]
        
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        converter.representative_dataset = representative_data_gen
        converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
        converter.inference_input_type = tf.uint8
        converter.inference_output_type = tf.uint8

    tflite_model = converter.convert()
    tflite_model_file = tflite_models_dir/ str(model_name+"-"+converter_name+".tflite")
    tflite_model_file.write_bytes(tflite_model)
    print("Model saved to : ", tflite_model_file)
    return tflite_model_file, converter_name


def main(args):
    """ 
    Description - Main function
    :type args: list
    :param args: List of arguments to parse
    """
    print("Enter model type to quantize - 1. ChexNet, 2. Pruned")
    model_type = int(input())
    if model_type == 1:
        model_name = "chexnet"
    elif model_type == 2:
        model_name = "pruned"
    else:
        raise Exception("Incorrect model name")

    if (model_name == "chexnet"):
        print("Enter path to saved chexnet weights file ")
        weightsfile = str(input())
        model = model_builder(weightsfile)
        model.compile( loss='categorical_crossentropy', optimizer=Adam(lr=1e-5), metrics=['accuracy'])
    elif model_name == "chexnet-pruned":
        print("Enter path to saved weights file (.h5 format) ")
        weightsfile = str(input())
        model = tf.keras.models.load_model(weightsfile)
        model.compile( loss='categorical_crossentropy', optimizer=Adam(lr=1e-5), metrics=['accuracy'])
    else:
        raise Exception("Incorrect model name")

    class_names = ['Atelectasis','Cardiomegaly','Effusion','Infiltration','Mass','Nodule','Pneumonia','Pneumothorax','Consolidation','Edema','Emphysema','Fibrosis','Pleural_Thickening','Hernia']
    
    tflite_models_dir = pathlib.Path(args.tflitepath)
    tflite_models_dir.mkdir(exist_ok=True, parents=True)

    test_sequence = AugmentedImageSequence(
        dataset_csv_file=os.path.join("data_split/dev.csv"),
        class_names=class_names,
        source_image_dir=args.imagesourcedir,
        batch_size=32,
        target_size=(224,224),
        augmenter=None,
        shuffle_on_epoch_end=False,
    )

    print("Enter conversion type \n 1. Normal tflite conversion \n 2. Dynamic quantization \n 3. Float-16 quantization \n 4. Int-8 quantization")
    conversion_type = int(input())
    tflite_model_file, converter_name = model_converter(model, conversion_type, tflite_models_dir, model_name, test_sequence)

    print("Model results - ")
    if(converter_name!="int8"):
        evaluate_tflite_model(tflite_model_file,test_sequence)
    else:
        evaluate_int8_tflite_model(tflite_model_file,test_sequence)        

    print("Done")
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('imagesourcedir', metavar="imagesourcedir", type=str, help="Path to images folder")
    parser.add_argument('tflitepath', metavar="tflitepath", type=str, help="Folder to store quantized models")
    args = parser.parse_args()
    main(args)