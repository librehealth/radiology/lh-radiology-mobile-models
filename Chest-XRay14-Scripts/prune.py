import argparse, os, tempfile
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_model_optimization as tfmot
from imgaug import augmenters as iaa
from tensorflow.keras.optimizers import Adam
from generator import AugmentedImageSequence
from model_build import model_builder
from evaluate_model import evaluate_model

def prune_model(model, train_sequence, val_sequence):
    
    """ 
    Description - Function to prune the model
    :type model: tensorflow model
    :param model: Model to be pruned
    
    :type train_sequence: generator
    :param train_sequence: Train data generator
    
    :type val_sequence: generator
    :param val_sequence: Validation data generator
    
    :rtype: tensorflow model
    """
    prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude

    batch_size = 32
    epochs = 10
    train_df = pd.read_csv("data_split/prune-train.csv")
    num_images = train_df.shape[0] 
    end_step = np.ceil(num_images / batch_size).astype(np.int32) * epochs

    # Define model for pruning.
    pruning_params = {'pruning_schedule': tfmot.sparsity.keras.PolynomialDecay(initial_sparsity=0.50,
    final_sparsity=0.80,
    begin_step=0,
    end_step=end_step)}

    model_for_pruning = prune_low_magnitude(model, **pruning_params)

    model_for_pruning.compile(optimizer='adam', 
    loss=tf.keras.losses.CategoricalCrossentropy(),
    metrics=['accuracy'])
    logdir = tempfile.mkdtemp()
    model_for_pruning.summary()
    callbacks_fn = [
        tfmot.sparsity.keras.UpdatePruningStep(),
        tfmot.sparsity.keras.PruningSummaries(log_dir=logdir)]
    
    model_for_pruning.fit(train_sequence,
    batch_size=batch_size, 
    epochs=epochs, 
    validation_data = val_sequence,
    callbacks=callbacks_fn)

    return model_for_pruning


def main(args):
    
    """ 
    Description - Main function
    :type args: list
    :param args: List of arguments to parse
    """
    model = model_builder(args.weightsfile)
    model.compile( loss='categorical_crossentropy', optimizer=Adam(lr=1e-5), metrics=['accuracy'])
    class_names = ['Atelectasis','Cardiomegaly','Effusion','Infiltration','Mass','Nodule','Pneumonia','Pneumothorax','Consolidation','Edema','Emphysema','Fibrosis','Pleural_Thickening','Hernia']

    
    augmenter = iaa.Sequential(
        [
            iaa.Fliplr(0.5),
        ],
        random_order=True,
        )
    train_sequence = AugmentedImageSequence(
        dataset_csv_file=os.path.join("data_split/prune-train.csv"),
        class_names=class_names,
        source_image_dir=args.imagesourcedir,
        batch_size=32,
        target_size=(224,224),
        augmenter=augmenter,
        shuffle_on_epoch_end=False,
    )

    val_sequence = AugmentedImageSequence(
        dataset_csv_file=os.path.join("data_split/dev.csv"),
        class_names=class_names,
        source_image_dir=args.imagesourcedir,
        batch_size=32,
        target_size=(224,224),
        augmenter=augmenter,
        shuffle_on_epoch_end=False,
    )

    pruned_model = prune_model(model,train_sequence, val_sequence)
    model_for_export = tfmot.sparsity.keras.strip_pruning(pruned_model)
    tf.keras.models.save_model(model_for_export, os.path.join(args.prunepath, str("chexnet-pruned"+".h5")), include_optimizer=False)
    
    print("Pruned model saved to : ",os.path.join(args.prunepath, str("chexnet-pruned"+".h5")))
    print("Model results - ")
    evaluate_model(model_for_export, val_sequence)
    print("Done")
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('weightsfile', metavar="weightsfile", type=str, help="Path to saved weights file")
    parser.add_argument('imagesourcedir', metavar="imagesourcedir", type=str, help="Path to images folder")
    parser.add_argument('prunepath', metavar="prunepath", type=str, help="Folder to store pruned models")
    args = parser.parse_args()
    main(args)