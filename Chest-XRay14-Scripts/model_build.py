from model_initialize import ModelInit
def model_builder(weightsfile):
    
    """ Description
    :type weightsfile: str
    :param weightsfile: Path to stored weights
    
    :rtype: tensorflow model
    """
    class_names = [ 'Atelectasis', 'Cardiomegaly', 'Effusion', 'Infiltration', 'Mass', 'Nodule', 'Pneumonia',
                'Pneumothorax', 'Consolidation', 'Edema', 'Emphysema', 'Fibrosis', 'Pleural_Thickening', 'Hernia']
    base_model_name = 'DenseNet121'
    model_weights_file = weightsfile #'../input/chexnetweights/brucechou1983_CheXNet_Keras_0.3.0_weights.h5'
    image_dimension = 224
    use_base_model_weights = True

    model_init = ModelInit()
    model = model_init.get_model(
        class_names,
        model_name=base_model_name,
        use_base_weights=use_base_model_weights,
        weights_path=model_weights_file,
        input_shape=(image_dimension, image_dimension, 3))

    return model