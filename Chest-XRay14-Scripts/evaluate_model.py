import numpy as np
import tensorflow as tf
from tensorflow.keras.optimizers import Adam
from generator import AugmentedImageSequence
from tqdm import tqdm
from model_build import model_builder
from sklearn.metrics import roc_auc_score
import os, argparse

def evaluate_model(model, test_sequence):
    
    """ Description - Function to evaluate model
    :type model: tf model
    :param model: Model to evaluate
    
    :type test_sequence: generator
    :param test_sequence: Data generator to evaluate model
    """
    predictions = model.predict_generator(test_sequence)
    true_values = test_sequence.get_y_true()
    get_aurocs(predictions, true_values)

def get_aurocs(predictions, true_values):
    
    """ Description - Function to get auroc scores
    :type predictions: numpy array
    :param predictions: predicted values from model
    
    :type true_values: numpy array
    :param true_values: true labels of data
    
    """
    class_names = ['Atelectasis','Cardiomegaly','Effusion','Infiltration','Mass','Nodule','Pneumonia','Pneumothorax','Consolidation','Edema','Emphysema','Fibrosis','Pleural_Thickening','Hernia']
    aurocs=[]
    for i in range(len(class_names)):
        score = roc_auc_score(true_values[:, i], predictions[:, i])
        aurocs.append(score)
        print(f"{class_names[i]}: {score}\n")
    print("Mean auroc : {}".format(np.mean(aurocs)))

def evaluate_tflite_model(model_path, test_sequence):
    """ Description - Function to evaluate tflite dynamic and fp16 models
    :type model: tf model
    :param model: Model to evaluate
    
    :type test_sequence: generator
    :param test_sequence: Data generator to evaluate model
    """
    interpreter = tf.lite.Interpreter(model_path=str(model_path))
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    
    predictions = []
    for batch in tqdm(iter(test_sequence)):
        for image_index in tqdm(range(batch[0].shape[0])): 
            val_image = np.expand_dims(batch[0][image_index], axis=0).astype(np.float32) 
            interpreter.set_tensor(input_details[0]['index'], val_image)
            interpreter.invoke()
            output = interpreter.get_tensor(output_details[0]['index'])
            predictions.append(output[0])
    
    predictions = np.vstack(predictions)
    true_values = test_sequence.get_y_true()
    get_aurocs(predictions, true_values)

def evaluate_int8_tflite_model(model_path, test_sequence):
    """ Description - Function to evaluate tflite int8 model
    :type model: tf model
    :param model: Model to evaluate
    
    :type test_sequence: generator
    :param test_sequence: Data generator to evaluate model
    """
    interpreter = tf.lite.Interpreter(model_path=str(model_path))
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    
    predictions = []
    for batch in tqdm(iter(test_sequence)):
        for image_index in tqdm(range(batch[0].shape[0])): 
            val_image = np.expand_dims(batch[0][image_index], axis=0).astype(np.uint8) 
            interpreter.set_tensor(input_details[0]['index'], val_image)
            interpreter.invoke()
            output = interpreter.get_tensor(output_details[0]['index'])
            predictions.append(output[0])
    
    predictions = np.vstack(predictions)
    true_values = test_sequence.get_y_true()
    get_aurocs(predictions, true_values)

def main(args):
    """ 
    Description - Main function
    :type args: list
    :param args: List of arguments to parse
    """
    class_names = ['Atelectasis','Cardiomegaly','Effusion','Infiltration','Mass','Nodule','Pneumonia','Pneumothorax','Consolidation','Edema','Emphysema','Fibrosis','Pleural_Thickening','Hernia']
    test_sequence = AugmentedImageSequence(
        dataset_csv_file=os.path.join("data_split/dev.csv"),
        class_names=class_names,
        source_image_dir=args.imagesourcedir,
        batch_size=32,
        target_size=(224,224),
        augmenter=None,
        shuffle_on_epoch_end=False,
    )

    print("Enter type of model to evaluate \n 1. Pretrained weights \n 2. Pruned model \n 3. TFLite dynamic/quantized model \n 4. Int8 Quantized model")
    model_type = int(input())
    if model_type == 1:
        print("Enter path to model")
        model_path = str(input())
        model = model_builder(model_path)
        model.compile( loss='categorical_crossentropy', optimizer=Adam(lr=1e-5), metrics=['accuracy'])
        evaluate_model(model, test_sequence)
    elif model_type == 2:
        print("Enter path to model")
        model_path = str(input())
        model = tf.keras.models.load_model(model_path)
        model.compile( loss='categorical_crossentropy', optimizer=Adam(lr=1e-5), metrics=['accuracy'])
        evaluate_model(model,test_sequence)
    elif model_type == 3:
        print("Enter path to model")
        model_path = str(input())
        evaluate_tflite_model(model_path,test_sequence)
    elif model_type == 4:
        print("Enter path to model")
        model_path = str(input())
        evaluate_int8_tflite_model(model_path,test_sequence)
    else:
        raise Exception("Incorrect model type")

    print("Done")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('imagesourcedir', metavar="imagesourcedir", type=str, help="Path to images folder")
    args = parser.parse_args()
    main(args) 