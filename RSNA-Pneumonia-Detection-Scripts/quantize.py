import tensorflow as tf
import pathlib, os, argparse
import train
import sys
import numpy as np
import pandas as pd
from tqdm import tqdm
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator

def batch_generator(train_df, val_df, test_df):
    
    """ 
    Description - Loads data and returns batches of images
    :type train_df: DataFrame
    :param train_df: Dataframe of training data
    
    :type val_df: DataFrame
    :param val_df: Dataframe of validation data
    
    :type test_df: DataFrame
    :param test_df: DataFrame of test data
    
    :rtype: DataFrameIterator
    """
    train_gen = ImageDataGenerator(
        shear_range=0.2,
        zoom_range=0.2,
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

    val_gen = ImageDataGenerator()

    test_gen = ImageDataGenerator()

    # Loading batch size of images for training and validation
    train_generator=train_gen.flow_from_dataframe(
        dataframe=train_df,
        x_col="patientId",
        y_col="Target",
        batch_size=train_df.shape[0],
        seed=42,
        shuffle=True,
        class_mode="categorical",
        target_size=(224,224))

    val_generator=val_gen.flow_from_dataframe(
        dataframe=val_df,
        x_col="patientId",
        y_col="Target",
        batch_size=val_df.shape[0],
        seed=42,
        shuffle=False,
        class_mode="categorical",
        target_size=(224,224))

    test_generator=test_gen.flow_from_dataframe(
        dataframe=test_df,
        x_col="patientId",
        y_col="Target",
        batch_size= test_df.shape[0],
        seed=42,
        shuffle=False,
        class_mode="categorical",
        target_size=(224,224))

    return train_generator, val_generator, test_generator

def model_converter(model, conversion_type, tflite_models_dir, model_name, val_generator):
    
    """ 
    Description - Converts model to quantized format
    :type model: tensorflow model
    :param model: Model to be converted
    
    :type conversion_type: int
    :param conversion_type: Type of quantized model to obtain
    
    :type tflite_models_dir: str
    :param tflite_models_dir: Directory to store the tflite model
    
    :type model_name: str
    :param model_name: Name of model to be converted
    
    :rtype: bytes, str
    """
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    if(conversion_type == 1):
        converter_name = "generic"
    elif(conversion_type == 2):
        converter_name = "dynamic"
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
    elif(conversion_type == 3):
        converter_name = "float16"
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        converter.target_spec.supported_types = [tf.float16]
    elif(conversion_type == 4):
        converter_name = "int8"
        val_images, _ = tqdm(next(iter(val_generator)))
        images = np.expand_dims(val_images[0], axis=0).astype(np.float32)
        val_ds = tf.data.Dataset.from_tensor_slices((images)).batch(1)

        def representative_data_gen():
            for input_value in val_ds.take(100):
                yield [input_value]
        
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        converter.representative_dataset = representative_data_gen
        converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
        converter.inference_input_type = tf.uint8
        converter.inference_output_type = tf.uint8

    tflite_model = converter.convert()
    tflite_model_file = tflite_models_dir/ str(model_name+"-"+converter_name+".tflite")
    tflite_model_file.write_bytes(tflite_model)
    print("Model saved to : ", tflite_model_file)
    return tflite_model, tflite_model_file

def evaluate_model(model_path, image_batch, label_batch):
    
    """ Description - Function to evaluate quantized model
    :type model_path: str
    :param model_path: Path of tflite model to evaluate
    
    :type image_batch: numpy array
    :param image_batch: Batch of images to evaluate the model
    
    :type label_batch: numpy array
    :param label_batch: Batch of labels corresponding to images to evaluate the model
    
    :rtype: float
    """
    interpreter = tf.lite.Interpreter(model_path=str(model_path))
    interpreter.allocate_tensors()
    input_index = interpreter.get_input_details()[0]["index"]
    output_index = interpreter.get_output_details()[0]["index"]

    # Run predictions on every image in the "test" dataset.
    prediction_images = []
    for image in tqdm(image_batch):
        # Pre-processing: add batch dimension and convert to float32 to match with the model's input data format.
        image = np.expand_dims(image, axis=0).astype(np.float32)
        interpreter.set_tensor(input_index, image)

        # Run inference.
        interpreter.invoke()

        # Post-processing: remove batch dimension and find the digit with highest probability.
        output = interpreter.tensor(output_index)
        result = np.argmax(output()[0])
        prediction_images.append(result)

    # Compare prediction results with ground truth labels to calculate accuracy.
    accurate_count = 0
    for index in tqdm(range(len(prediction_images))):
        if prediction_images[index] == np.where(label_batch[index] == 1)[0][0]:
            accurate_count += 1
    accuracy = accurate_count * 1.0 / len(prediction_images)

    return accuracy

def main(args):
    """ 
    Description - Main function
    :type args: list
    :param args: List of arguments to parse
    """

    print("Enter model type to quantize - 1. Densenet, 2. Inception, 3. Pruned")
    model_type = int(input())
    if model_type == 1:
        model_name = "densenet"
    elif model_type == 2:
        model_name = "inception"
    elif model_type == 3:
        model_name = "pruned"
    else:
        raise Exception("Incorrect model name")

    if (model_name == "densenet" or model_name == "inception"):
        # weights.hdf5 or model.h5 is the expected input here
        model = train.build_model(model_name)
        print("Enter path to saved weights file (.hdf5 or .h5 format) ")
        weightsfile = str(input())
        model.load_weights(weightsfile)
    elif model_name == "pruned":
        #model.h5 is the expected input here
        print("Enter path to saved weights file (.h5 format) ")
        weightsfile = str(input())
        model = tf.keras.models.load_model(weightsfile)
        model.compile( loss='categorical_crossentropy', optimizer=Adam(lr=1e-5), metrics=['accuracy'])
    else:
        raise Exception("Incorrect model name")

    tflite_models_dir = pathlib.Path(args.tflitepath)
    tflite_models_dir.mkdir(exist_ok=True, parents=True)

    train_df = pd.read_csv("train_df.csv")
    val_df = pd.read_csv("val_df.csv")
    test_df = pd.read_csv("test_df.csv")
    train_df['Target'] = train_df['Target'].astype('str')
    val_df['Target'] = val_df['Target'].astype('str')
    test_df['Target'] = test_df['Target'].astype('str')
    _, val_generator, test_generator = batch_generator(train_df, val_df, test_df)

    print("Enter conversion type \n 1. Normal tflite conversion \n 2. Dynamic quantization \n 3. Float-16 quantization \n 4. Int-8 quantization")
    conversion_type = int(input())
    _, tflite_model_file = model_converter(model, conversion_type, tflite_models_dir, model_name, val_generator)

    image_batch, label_batch = next(iter(val_generator))
    print("Model results on validation batch : ")
    print(evaluate_model(tflite_model_file, image_batch, label_batch))

    image_batch, label_batch = next(iter(test_generator))
    print("Model results on test batch : ")
    print(evaluate_model(tflite_model_file, image_batch, label_batch))

    print("Done")
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    #parser.add_argument('weightsfile', metavar="weightsfile", type=str, help="Path to saved weights file")
    parser.add_argument('tflitepath', metavar="tflitepath", type=str, help="Folder to store quantized models")
    args = parser.parse_args()
    main(args)