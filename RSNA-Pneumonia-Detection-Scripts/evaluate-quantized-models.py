import tensorflow as tf
import pathlib, argparse
import tqdm
import numpy as np
import pandas as pd
import train
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import quantize

def evaluate_model(model_path, image_batch, label_batch):
    
    """ Description - Function to evaluate quantized model
    :type model_path: str
    :param model_path: Path of tflite model to evaluate
    
    :type image_batch: numpy array
    :param image_batch: Batch of images to evaluate the model
    
    :type label_batch: numpy array
    :param label_batch: Batch of labels corresponding to images to evaluate the model
    
    :rtype: float
    """
    interpreter = tf.lite.Interpreter(model_path=str(model_path))
    interpreter.allocate_tensors()
    input_index = interpreter.get_input_details()[0]["index"]
    output_index = interpreter.get_output_details()[0]["index"]

    # Run predictions on every image in the "test" dataset.
    prediction_images = []
    for image in image_batch:
        # Pre-processing: add batch dimension and convert to float32 to match with the model's input data format.
        image = np.expand_dims(image, axis=0).astype(np.float32)
        interpreter.set_tensor(input_index, image)

        # Run inference.
        interpreter.invoke()

        # Post-processing: remove batch dimension and find the digit with highest probability.
        output = interpreter.tensor(output_index)
        result = np.argmax(output()[0])
        prediction_images.append(result)

    # Compare prediction results with ground truth labels to calculate accuracy.
    accurate_count = 0
    for index in range(len(prediction_images)):
        if prediction_images[index] == np.where(label_batch[index] == 1)[0][0]:
            accurate_count += 1
    accuracy = accurate_count * 1.0 / len(prediction_images)

    return accuracy

def main(args):
    """ 
    Description - Main function
    :type args: list
    :param args: List of arguments to parse
    """
    tflite_model_file = args.tflitepath
    train_df = pd.read_csv("train_df.csv")
    val_df = pd.read_csv("val_df.csv")
    test_df = pd.read_csv("test_df.csv")
    train_df['Target'] = train_df['Target'].astype('str')
    val_df['Target'] = val_df['Target'].astype('str')
    test_df['Target'] = test_df['Target'].astype('str')
    _, val_generator, test_generator = quantize.batch_generator(train_df, val_df, test_df)

    image_batch, label_batch = next(iter(val_generator))
    print("Model results on validation batch : ")
    print(evaluate_model(tflite_model_file, image_batch, label_batch))

    image_batch, label_batch = next(iter(test_generator))
    print("Model results on test batch : ")
    print(evaluate_model(tflite_model_file, image_batch, label_batch))

    print("Done")
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('tflitepath', metavar="tflitepath", type=str, help="Folder to stored quantized models")
    args = parser.parse_args()
    main(args)  